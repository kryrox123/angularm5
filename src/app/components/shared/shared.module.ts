import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';

//angular material



@NgModule({
  declarations: [
    NavbarComponent
  ],
  imports: [
    CommonModule
  ]

})
export class SharedModule { }
