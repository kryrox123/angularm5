import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form5',
  templateUrl: './form5.component.html',
  styleUrls: ['./form5.component.css']
})
export class FormComponent implements OnInit {

  form!: FormGroup;
  name!: string;
  datos!: string;

  constructor(private fb: FormBuilder) { 
    this.crearForm();
  }

  
  get pasatiemposO() {
    return this.form.get('pasatiempos') as FormArray;
    }

  ngOnInit(): void {
  }  
  
  crearForm(): void{
    this.form = this.fb.group({
      nombre: ['', Validators.required],
      pasatiempos: this.fb.array([
      ])
    })
  }

  adicionar(): void{
    console.log('ADICIONAR');
    console.log(this.form.value.nombre);
    
    this.name = this.form.value.nombre
    

    this.pasatiemposO.push(this.fb.control(''))
   }

   eliminarCheck(i: number): void{
    this.pasatiemposO.removeAt(i)
  }

  vaciarcaja():void {
    console.log(this.datos);
    
    this.datos = ''
  }

  guardar(): void{
    console.log('guardado');
    this.datos = 'Id:' + this.name + ' - Nombre:' + this.name
  }
}
